#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Jun 2019
Based on a demo by Slava Korolev:
    https://towardsdatascience.com/today-im-going-to-talk-about-a-small-practical-example-of-using-neural-networks-training-one-to-6b2cbd6efdb3
"""
from random import randint

class SnakeGame:
    " Implements the snake game core"

    def __init__(self, width, height):
        "Initialize board"
        self.width = width
        self.height = height
        self.HORIZONTAL = 0
        self.VERTICAL = 0
        self.UP = 0
        self.RIGHT = 1
        self.DOWN = 2
        self.LEFT = 3
        self.score = 0
        self.done = False
        self.create_snake()
        self.create_apple()

    def create_apple(self):
        "create a new apple away from the snake"
        self.apple = ( randint(0,self.width-1), randint(0,self.height-1) )
        while self.apple in self.snake:
            self.apple = ( randint(0,self.width-1), randint(0,self.height-1) )

    def create_snake(self):
        "create a snake, size 3, at random position and orientation"
        x = randint( 5, self.width-5 )   # not to close to border
        y = randint( 5, self.height-5 )
        orient = randint(0,1)
        self.snake = []
        for i in range(3):
            if orient == 0:
                x = x+1
            else:
                y = y+1
            self.snake.append( (x,y) )

    def grow_snake(self, d):
        "add one position to snake head (0=up, 1=right, 2=down, 3=left)"
        x,y = self.snake[0]
        if d == self.UP:
            y = y-1
        elif d == self.RIGHT:
            x = x+1
        elif d == self.DOWN:
            y = y+1
        else: # self.LEFT
            x = x-1
        self.snake.insert(0,(x,y))

    def check_collisions(self):
        "check if game is over by colliding with edge or itself"
        # just need to check snake's head
        x,y = self.snake[0]
        if (x == -1 or x == self.width 
            or y == -1 or y == self.height 
            or (x,y) in self.snake[1:]):
            self.done = True

    def step(self, direction):
        "move snake/game one step"
        self.grow_snake(direction)  # two steps: grow+remove last
        if self.snake[0] == self.apple:
            self.score = self.score+1
            self.create_apple()     # new apple
        else:
            self.snake.pop()
            self.check_collisions()


    def print_state(self):
        "print the current board state"
        for i in range(self.height):
            for j in range(self.width):
                if (j,i) == self.apple:
                    print('A', end ="")
                elif (j,i) == self.snake[0]:
                    print('O', end ="")
                elif (j,i) in self.snake[1:]:
                    print('X', end ="")
                else:
                    print('-', end ="")
            print()

    def test_step(self, direction):
        "to test: move the snake and print the game state"
        self.step(direction)
        self.print_state()
        if self.done:
            print("Game over! Score=",self.score)


game = SnakeGame(20,20)
game.print_state()
			
